﻿using Rhino;
using Rhino.Commands;
using Rhino.Geometry;
using Rhino.Input;
using Rhino.Input.Custom;

using DolphinMesh;

namespace RhinoSupport
{
    public static partial class Support
    {

        public static Point3d ToPoint3d(this Vertex vertex)
        {
            return new Point3d(vertex.position[0], vertex.position[1], vertex.position[2]);
        }

        public static Mesh ToRhinoMesh(this TriMesh dolphinMesh)
        {
            Mesh rhinoMesh = new Mesh();

            // Add vertices to rhinoMesh
            foreach (var vertex in dolphinMesh.vertices())
            {
                rhinoMesh.Vertices.Add(vertex.ToPoint3d());
            }

            // Add faces to rhinoMesh
            foreach (var face in dolphinMesh.faces())
            {
                rhinoMesh.Faces.AddFace(
                    face.get_vertex(0).index,
                    face.get_vertex(1).index,
                    face.get_vertex(2).index);
            }

            // Compute the surface normals and return mesh
            rhinoMesh.Normals.ComputeNormals();

            return rhinoMesh;
        }

        public static TriMesh ToDolphinMesh(this Mesh rhinoMesh)
        {
            // create TriMesh
            int n_vertices = rhinoMesh.Vertices.Count;
            int n_faces = rhinoMesh.Faces.Count;
            var dolphinMesh = new TriMesh(n_faces, n_vertices, 1.1);

            // format Rhino mesh object
            rhinoMesh.Vertices.CombineIdentical(true, true);
            rhinoMesh.Vertices.CullUnused();
            rhinoMesh.UnifyNormals();
            rhinoMesh.Weld(Math.PI);

            // Add vertices to dolphinMesh
            foreach (Point3f vertex in rhinoMesh.Vertices)
            {
                dolphinMesh.add_vertex(vertex.X, vertex.Y, vertex.Z);
            }

            // Add faces to dolphinMesh
            foreach (var face in rhinoMesh.Faces)
            {
                dolphinMesh.add_face(face.A, face.B, face.C);
            }

            return dolphinMesh;
        }
    }
}
