﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace DolphinMesh
{
    public static partial class External
    {
        // default constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_default_constructor")]
        public static extern IntPtr quadmesh_default_constructor(IntPtr error);

        // specialized constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_construct_f1")]
        public static extern IntPtr quadmesh_construct_f1(int n_faces, int n_vertices, double storage_factor, IntPtr error);

        // copy constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_copy_constructor")]
        public static extern IntPtr quadmesh_copy_constructor(IntPtr other, IntPtr error);

        // move constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_move_constructor")]
        public static extern IntPtr quadmesh_move_constructor(IntPtr other, IntPtr error);

        // copy assignment
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_copy_assignment")]
        public static extern void quadmesh_copy_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // move assignment
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_move_assignment")]
        public static extern void quadmesh_move_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // destructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_destruct")]
        public static extern void quadmesh_destruct(IntPtr _opaque);

        // properties
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_vertices")]
        public static extern int quadmesh_n_vertices(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_edges")]
        public static extern int quadmesh_n_edges(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_faces")]
        public static extern int quadmesh_n_faces(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_boundary_components")]
        public static extern int quadmesh_n_boundary_components(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_genus")]
        public static extern int quadmesh_n_genus(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_n_euler")]
        public static extern int quadmesh_n_euler(IntPtr quadmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_is_closed")]
        public static extern bool quadmesh_is_closed(IntPtr quadmesh_t);

        // construct mesh
        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_add_vertex")]
        public static extern void quadmesh_add_vertex(IntPtr quadmesh, double x, double y, double z, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_add_face_4")]
        public static extern void quadmesh_add_face_4(IntPtr quadmesh, int a, int b, int c, int d, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_finalize")]
        public static extern void quadmesh_finalize(IntPtr quadmesh, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "quadmesh_get_vertex_position")]
        public static extern IntPtr quadmesh_get_vertex_position(IntPtr quadmesh, int index, IntPtr error);

    }
    public class QuadMesh
    {
        IntPtr _opaque;

        public QuadMesh() 
        {
            _opaque = External.quadmesh_default_constructor(IntPtr.Zero);
        }

        private QuadMesh(IntPtr quadmesh)
        {
            _opaque = quadmesh;
        }

        public QuadMesh(int n_faces, int n_vertices, double storage_factor)
        {
            _opaque = External.quadmesh_construct_f1(n_faces, n_vertices, storage_factor, IntPtr.Zero);
        }

        ~QuadMesh()
        {
            External.quadmesh_destruct(_opaque);
        }

        public int n_vertices()
        {
            return External.quadmesh_n_vertices(_opaque);
        }
        public int n_edges()
        {
            return External.quadmesh_n_edges(_opaque);
        }
        public int n_faces()
        {
            return External.quadmesh_n_faces(_opaque);
        }

        public int n_boundary_components()
        {
            return External.quadmesh_n_boundary_components(_opaque);
        }

        public int n_genus()
        {
            return External.quadmesh_n_genus(_opaque);
        }

        public int n_euler()
        {
            return External.quadmesh_n_euler(_opaque);
        }

        public bool is_closed()
        {
            return External.quadmesh_is_closed(_opaque);
        }

        public void add_vertex(double x, double y, double z)
        {
            External.quadmesh_add_vertex(_opaque, x, y, z, IntPtr.Zero);
        }

        public void add_face(int a, int b, int c, int d)
        {
            External.quadmesh_add_face_4(_opaque, a, b, c, d, IntPtr.Zero);
        }

        public void finalize()
        {
            External.quadmesh_finalize(_opaque, IntPtr.Zero);
        }

        public Vertex get_vertex(int index)
        {
            IntPtr p = External.quadmesh_get_vertex_position(_opaque, index, IntPtr.Zero);
            unsafe
            {
                var s = new Span<double>(p.ToPointer(), 3);
                return new Vertex(index, new Point3<double>(s[0], s[1], s[2]));
            }
        }

        public QuadMesh deep_copy()
        {
            return new QuadMesh(External.quadmesh_copy_constructor(_opaque, IntPtr.Zero));
        }

        public QuadMesh shallow_copy()
        {
            return new QuadMesh(_opaque);
        }
    }
    
}
