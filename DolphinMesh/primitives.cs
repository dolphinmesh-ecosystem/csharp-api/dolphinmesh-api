﻿using System;
using System.Runtime.InteropServices;
using System.Collections.Generic;

namespace DolphinMesh
{

    [StructLayout(LayoutKind.Sequential)]
    unsafe public struct View<T> where T : unmanaged
    {
        private unsafe T* ptr;
        private int length;

        public View(T* first, int len)
        {
            ptr = first;
            length = len;
        }

        public T this[int index]
        {
            get
            {
                if (index<0 || index > length)
                {
                    throw new IndexOutOfRangeException("Index out of range.");
                }
                return ptr[index];
            }
            set
            {
                if (index < 0 || index > length)
                {
                    throw new IndexOutOfRangeException("Index out of range.");
                }
                ptr[index] = value;
            }
        }

        public static bool operator ==(View<T> a, View<T> b)
        {
            return (a.ptr == b.ptr && a.length == b.length);
        }

        public static bool operator !=(View<T> a, View<T> b)
        {
            return !(a == b);
        }
    }


    public class Vertex
    {
        public readonly int index;
        public View<double> position;

        public Vertex(int i, View<double> p)
        {
            index = i;
            position = p;
        }

        public static bool operator ==(Vertex a, Vertex b)
        {
            return a.position == b.position && a.index == b.index;
        }

        public static bool operator !=(Vertex a, Vertex b)
        {
            return !(a == b);
        }
    }

    public class Face
    {
        public readonly int index;
        public Vertex[] vertices;

        public Face(int i, params Vertex[] v)
        {
            index = i;
            vertices = v;
        }

        public int n_sides() => vertices.Length;

        public Vertex get_vertex(int index) => vertices[index];
    }

    public struct Edge
    {
        public readonly int index;
        public Vertex[] vertices;

        public Edge(int i, Vertex va, Vertex vb)
        {
            index = i;
            vertices = new Vertex[2]{ va, vb};
        }

        public Vertex get_vertex(int index) => vertices[index];
    }

    public struct DirectedEdge
    {
        public readonly int index;
        public Vertex[] vertices;

        public DirectedEdge(int i, Vertex va, Vertex vb)
        {
            index = i;
            vertices = new Vertex[2] { va, vb };
        }

        public Vertex get_vertex(int index) => vertices[index];
    }


    public class Chain
    {
        IntPtr _opaque;

        public Chain(IntPtr c)
        {
            _opaque = c;
        }

        ~Chain() 
        {
            if (_opaque != IntPtr.Zero)
                External.chain_destruct(_opaque); 
        }


        public bool is_closed()
        {
            return External.chain_is_closed(_opaque);
        }

        public int n_vertices()
        {
            return External.chain_n_vertices(_opaque);
        }

        public int n_edges()
        {
            return External.chain_n_edges(_opaque);
        }

        public Vertex get_vertex(int index)
        {
            unsafe
            {
                double* p = External.chain_get_vertex_position_ptr(_opaque, index);
                return new Vertex(index, new View<double>(p, 3));
            }
        }

        public DirectedEdge get_edge(int index)
        {
            if (index == n_edges() - 1 && is_closed())
            {
                Vertex a = get_vertex(index);
                Vertex b = get_vertex(0);
                return new DirectedEdge(index, a, b);
            }
            else
            {
                Vertex a = get_vertex(index);
                Vertex b = get_vertex(index + 1);
                return new DirectedEdge(index, a, b);
            }
        }

        public IEnumerable<Vertex> Vertices()
        {
            for (int k=0; k<n_vertices(); ++k)
            {
                yield return get_vertex(k);
            }
        } 

    }
}
