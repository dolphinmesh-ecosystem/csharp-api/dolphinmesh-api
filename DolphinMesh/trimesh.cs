﻿using System;
using System.Runtime.InteropServices;

using System.Collections.Generic;

namespace DolphinMesh
{

    public static partial class External
    {
        // default constructor
        [DllImport("dolphinmesh-api.dll")]
        public static extern IntPtr trimesh_default_constructor(IntPtr error);

        // specialized constructor
        [DllImport("dolphinmesh-api.dll")]
        public static extern IntPtr trimesh_construct_f1(int n_faces, int n_vertices, double storage_factor, IntPtr error);

        // copy constructor
        [DllImport("dolphinmesh-api.dll")]
        public static extern IntPtr trimesh_copy_constructor(IntPtr other, IntPtr error);

        // move constructor
        [DllImport("dolphinmesh-api.dll")]
        public static extern IntPtr trimesh_move_constructor(IntPtr other, IntPtr error);

        // copy assignment
        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_copy_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // move assignment
        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_move_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // destructor
        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_destruct(IntPtr _opaque);

        // properties
        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_vertices(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_edges(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_faces(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_boundary_components(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_genus(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int trimesh_n_euler(IntPtr trimesh);

        [DllImport("dolphinmesh-api.dll")]
        public static extern bool trimesh_is_closed(IntPtr trimesh_t);

        // construct mesh
        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_add_vertex(IntPtr trimesh, double x, double y, double z, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_add_face_3(IntPtr trimesh, int a, int b, int c, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_finalize(IntPtr trimesh, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_get_vertex_position(IntPtr trimesh, int index, double[] position, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        unsafe public static extern double* trimesh_get_vertex_position_ptr(IntPtr trimesh, int index);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_get_face_vertices_3(IntPtr trimesh, int index, int[] fv, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void trimesh_get_edge_vertices(IntPtr trimesh, int index, int[] fv, IntPtr error);

        [DllImport("dolphinmesh-api.dll")]
        public static extern IntPtr trimesh_get_boundary_component(IntPtr trimesh, int index, IntPtr error);

        // chain
        [DllImport("dolphinmesh-api.dll")]
        public static extern bool chain_is_closed(IntPtr chain);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int chain_n_vertices(IntPtr chain);

        [DllImport("dolphinmesh-api.dll")]
        public static extern int chain_n_edges(IntPtr chain);

        [DllImport("dolphinmesh-api.dll")]
        public static extern void chain_destruct(IntPtr chain);

        [DllImport("dolphinmesh-api.dll")]
        unsafe public static extern double* chain_get_vertex_position_ptr(IntPtr chain, int index);


    }
    public class TriMesh
    {
        IntPtr _opaque;

        public TriMesh()
        {
            _opaque = External.trimesh_default_constructor(IntPtr.Zero);
        }

        private TriMesh(IntPtr trimesh)
        {
            _opaque = trimesh;
        }

        public TriMesh(int n_faces, int n_vertices, double storage_factor)
        {
            _opaque = External.trimesh_construct_f1(n_faces, n_vertices, storage_factor, IntPtr.Zero);
        }

        ~TriMesh()
        {
            if (_opaque != IntPtr.Zero)
                External.trimesh_destruct(_opaque);
        }

        public int n_vertices()
        {
            return External.trimesh_n_vertices(_opaque);
        }
        public int n_edges()
        {
            return External.trimesh_n_edges(_opaque);
        }
        public int n_faces()
        {
            return External.trimesh_n_faces(_opaque);
        }

        public int n_boundary_components()
        {
            return External.trimesh_n_boundary_components(_opaque);
        }

        public int n_genus()
        {
            return External.trimesh_n_genus(_opaque);
        }

        public int n_euler()
        {
            return External.trimesh_n_euler(_opaque);
        }

        public bool is_closed()
        {
            return External.trimesh_is_closed(_opaque);
        }

        public void add_vertex(double x, double y, double z)
        {
            External.trimesh_add_vertex(_opaque, x, y, z, IntPtr.Zero);
        }

        public void add_face(int a, int b, int c)
        {
            External.trimesh_add_face_3(_opaque, a, b, c, IntPtr.Zero);
        }

        public void finalize()
        {
            External.trimesh_finalize(_opaque, IntPtr.Zero);
        }

        public Vertex get_vertex(int index)
        {
            unsafe
            {
                double* p = External.trimesh_get_vertex_position_ptr(_opaque, index);
                return new Vertex(index, new View<double>(p, 3));
            }
        }

        public Face get_face(int index)
        {
            int[] fv = new int[3];
            External.trimesh_get_face_vertices_3(_opaque, index, fv, IntPtr.Zero);
            return new Face(index, get_vertex(fv[0]), get_vertex(fv[1]), get_vertex(fv[2]));
        }

        public Edge get_edge(int index)
        {
            int[] ev = new int[2];
            External.trimesh_get_edge_vertices(_opaque, index, ev, IntPtr.Zero);
            return new Edge(index, get_vertex(ev[0]), get_vertex(ev[1]));
        }

        public Chain get_boundary_component(int index)
        {
            return new Chain(External.trimesh_get_boundary_component(_opaque, index, IntPtr.Zero));
        }

        public TriMesh deep_copy()
        {
            return new TriMesh(External.trimesh_copy_constructor(_opaque, IntPtr.Zero));
        }

        public TriMesh shallow_copy()
        {
            return new TriMesh(_opaque);
        }

        public IEnumerable<Vertex> vertices()
        {
            for (int k = 0; k < n_vertices(); ++k)
            {
                yield return get_vertex(k);
            }
        }

        public IEnumerable<Face> faces()
        {
            for (int k = 0; k < n_faces(); ++k)
            {
                yield return get_face(k);
            }
        }

        public IEnumerable<Edge> edges()
        {
            for (int k = 0; k < n_edges(); ++k)
            {
                yield return get_edge(k);
            }
        }

        public IEnumerable<Chain> Boundary()
        {
            for (int k = 0; k < n_boundary_components(); ++k)
            {
                yield return get_boundary_component(k);
            }
        }
    }
    
}
