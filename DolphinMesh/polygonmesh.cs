﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Runtime.InteropServices;

namespace DolphinMesh
{
    public static partial class External
    {
        // default constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_default_constructor")]
        public static extern IntPtr polygonmesh_default_constructor(IntPtr error);

        // specialized constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_construct_f1")]
        public static extern IntPtr polygonmesh_construct_f1(int n_faces, int n_vertices, double storage_factor, IntPtr error);

        // copy constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_copy_constructor")]
        public static extern IntPtr polygonmesh_copy_constructor(IntPtr other, IntPtr error);

        // move constructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_move_constructor")]
        public static extern IntPtr polygonmesh_move_constructor(IntPtr other, IntPtr error);

        // copy assignment
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_copy_assignment")]
        public static extern void polygonmesh_copy_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // move assignment
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_move_assignment")]
        public static extern void polygonmesh_move_assignment(IntPtr mesh, IntPtr other, IntPtr error);

        // destructor
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_destruct")]
        public static extern void polygonmesh_destruct(IntPtr _opaque);

        // properties
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_vertices")]
        public static extern int polygonmesh_n_vertices(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_edges")]
        public static extern int polygonmesh_n_edges(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_faces")]
        public static extern int polygonmesh_n_faces(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_boundary_components")]
        public static extern int polygonmesh_n_boundary_components(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_genus")]
        public static extern int polygonmesh_n_genus(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_n_euler")]
        public static extern int polygonmesh_n_euler(IntPtr polygonmesh);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_is_closed")]
        public static extern bool polygonmesh_is_closed(IntPtr polygonmesh_t);

        // construct mesh
        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_add_vertex")]
        public static extern void polygonmesh_add_vertex(IntPtr polygonmesh, double x, double y, double z, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_add_face_3")]
        public static extern void polygonmesh_add_face_3(IntPtr polygonmesh, int a, int b, int c, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_add_face_4")]
        public static extern void polygonmesh_add_face_4(IntPtr polygonmesh, int a, int b, int c, int d, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_add_face_5")]
        public static extern void polygonmesh_add_face_5(IntPtr polygonmesh, int a, int b, int c, int d, int e, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_finalize")]
        public static extern void polygonmesh_finalize(IntPtr polygonmesh, IntPtr error);

        [DllImport("dolphinmesh-api.dll", EntryPoint = "polygonmesh_get_vertex_position")]
        public static extern IntPtr polygonmesh_get_vertex_position(IntPtr polygonmesh, int index, IntPtr error);


    }
    public class PolygonMesh
    {
        IntPtr _opaque;

        public PolygonMesh() 
        {
            _opaque = External.polygonmesh_default_constructor(IntPtr.Zero);
        }

        private PolygonMesh(IntPtr polygonmesh)
        {
            _opaque = polygonmesh;
        }

        public PolygonMesh(int n_faces, int n_vertices, double storage_factor)
        {
            _opaque = External.polygonmesh_construct_f1(n_faces, n_vertices, storage_factor, IntPtr.Zero);
        }

        ~PolygonMesh()
        {
            External.polygonmesh_destruct(_opaque);
        }

        public int n_vertices()
        {
            return External.polygonmesh_n_vertices(_opaque);
        }
        public int n_edges()
        {
            return External.polygonmesh_n_edges(_opaque);
        }
        public int n_faces()
        {
            return External.polygonmesh_n_faces(_opaque);
        }

        public int n_boundary_components()
        {
            return External.polygonmesh_n_boundary_components(_opaque);
        }

        public int n_genus()
        {
            return External.polygonmesh_n_genus(_opaque);
        }

        public int n_euler()
        {
            return External.polygonmesh_n_euler(_opaque);
        }

        public bool is_closed()
        {
            return External.polygonmesh_is_closed(_opaque);
        }

        public void add_vertex(double x, double y, double z)
        {
            External.polygonmesh_add_vertex(_opaque, x, y, z, IntPtr.Zero);
        }

        public void add_face(int a, int b, int c)
        {
            External.polygonmesh_add_face_3(_opaque, a, b, c, IntPtr.Zero);
        }
        public void add_face(int a, int b, int c, int d)
        {
            External.polygonmesh_add_face_4(_opaque, a, b, c, d, IntPtr.Zero);
        }
        public void add_face(int a, int b, int c, int d, int e)
        {
            External.polygonmesh_add_face_5(_opaque, a, b, c, d, e, IntPtr.Zero);
        }

        public void finalize()
        {
            External.polygonmesh_finalize(_opaque, IntPtr.Zero);
        }

        public Vertex get_vertex(int index)
        {
            IntPtr p = External.polygonmesh_get_vertex_position(_opaque, index, IntPtr.Zero);
            unsafe
            {
                var s = new Span<double>(p.ToPointer(), 3);
                return new Vertex(index, new Point3<double>(s[0], s[1], s[2]));
            }
        }
        public PolygonMesh deep_copy()
        {
            return new PolygonMesh(External.polygonmesh_copy_constructor(_opaque, IntPtr.Zero));
        }

        public PolygonMesh shallow_copy()
        {
            return new PolygonMesh(_opaque);
        }
    }
    
}
