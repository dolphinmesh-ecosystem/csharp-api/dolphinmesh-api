using NUnit.Framework;

using DolphinMesh;

namespace TestDolphinMesh
{
    public class TestQuadMesh
    {
        QuadMesh mesh;

        [SetUp]
        public void Setup()
        {
            mesh = new QuadMesh(6, 8, 1.2);

            mesh.add_vertex(0.0, 0.0, 0.0); // 0
            mesh.add_vertex(1.0, 0.0, 0.0); // 1
            mesh.add_vertex(1.0, 1.0, 0.0); // 2
            mesh.add_vertex(0.0, 1.0, 0.0); // 3
            mesh.add_vertex(0.0, 0.0, 1.0); // 4
            mesh.add_vertex(1.0, 0.0, 1.0); // 5
            mesh.add_vertex(1.0, 1.0, 1.0); // 6
            mesh.add_vertex(0.0, 1.0, 1.0); // 7

            mesh.add_face(0, 3, 2, 1); // 1
            mesh.add_face(0, 1, 5, 4); // 2
            mesh.add_face(1, 2, 6, 5); // 3
            mesh.add_face(2, 3, 7, 6); // 4
            mesh.add_face(0, 4, 7, 3); // 5
            mesh.add_face(4, 5, 6, 7); // 6

            mesh.finalize();
        }


        [Test]
        public void TestCreation()
        {
            Assert.AreEqual(mesh.n_faces(), 6);
            Assert.AreEqual(mesh.n_vertices(), 8);
            Assert.AreEqual(mesh.n_edges(), 12);
        }

        [Test]
        public void TestTopology()
        {
            Assert.True(mesh.is_closed());
            Assert.AreEqual(mesh.n_genus(), 0);
            Assert.AreEqual(mesh.n_euler(), 2);
            Assert.AreEqual(mesh.n_boundary_components(), 0);
        }

        [Test]
        public void TestVertexGetter()
        {
            Vertex a = mesh.get_vertex(0);
            Assert.True(a.index == 0);
            Assert.AreEqual(a.position.x, 0);
            Assert.AreEqual(a.position.y, 0);
            Assert.AreEqual(a.position.z, 0);

            Vertex b = mesh.get_vertex(1);
            Assert.True(b.index == 1);
            Assert.AreEqual(b.position.x, 1);
            Assert.AreEqual(b.position.y, 0);
            Assert.AreEqual(b.position.z, 0);
        }
    }
}