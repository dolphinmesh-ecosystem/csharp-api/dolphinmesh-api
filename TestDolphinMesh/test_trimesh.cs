using NUnit.Framework;

using DolphinMesh;

namespace TestDolphinMesh
{
    public class TestTriMesh
    {
        TriMesh mesh;

        [SetUp]
        public void Setup()
        {
            mesh = new TriMesh(4, 4, 1.2);

            mesh.add_vertex(0.0, 0.0, 0.0);  // 0
            mesh.add_vertex(1.0, 0.0, 0.0);  // 1
            mesh.add_vertex(0.0, 1.0, 0.0);  // 2
            mesh.add_vertex(0.0, 0.0, 1.0);  // 3

            mesh.add_face(0, 2, 1);
            mesh.add_face(0, 1, 3);
            mesh.add_face(1, 2, 3);

            mesh.finalize();
        }


        [Test]
        public void TestCreation()
        {
            Assert.AreEqual(mesh.n_faces(), 3);
            Assert.AreEqual(mesh.n_vertices(), 4);
            Assert.AreEqual(mesh.n_edges(), 6);
        }

        [Test]
        public void TestTopology()
        {
            Assert.False(mesh.is_closed());
            Assert.AreEqual(mesh.n_genus(), 0);
            Assert.AreEqual(mesh.n_euler(), 1);
            Assert.AreEqual(mesh.n_boundary_components(), 1);
        }

        [Test]
        public void TestGetVertex()
        {
            Vertex a = mesh.get_vertex(0);
            Assert.True(a.index == 0);
            Assert.AreEqual(a.position[0], 0);
            Assert.AreEqual(a.position[1], 0);
            Assert.AreEqual(a.position[2], 0);

            Vertex b = mesh.get_vertex(1);
            Assert.True(b.index == 1);
            Assert.AreEqual(b.position[0], 1);
            Assert.AreEqual(b.position[1], 0);
            Assert.AreEqual(b.position[2], 0);

            Vertex c = mesh.get_vertex(2);
            Assert.True(c.index == 2);
            Assert.AreEqual(c.position[0], 0);
            Assert.AreEqual(c.position[1], 1);
            Assert.AreEqual(c.position[2], 0);

            Vertex d = mesh.get_vertex(3);
            Assert.True(d.index == 3);
            Assert.AreEqual(d.position[0], 0);
            Assert.AreEqual(d.position[1], 0);
            Assert.AreEqual(d.position[2], 1);
        }


        [Test]
        public void TestGetFace()
        {
            // face properties
            Face f = mesh.get_face(1);
            Assert.AreEqual(f.index, 1);
            Assert.AreEqual(f.n_sides(), 3);

            // check vertices
            Vertex a = f.get_vertex(0);
            Assert.AreEqual(a.position[0], 1);
            Assert.AreEqual(a.position[1], 0);
            Assert.AreEqual(a.position[2], 0);

            Vertex b = f.get_vertex(1);
            Assert.AreEqual(b.position[0], 0);
            Assert.AreEqual(b.position[1], 0);
            Assert.AreEqual(b.position[2], 0);

            Vertex c = f.get_vertex(2);
            Assert.AreEqual(c.position[0], 0);
            Assert.AreEqual(c.position[1], 0);
            Assert.AreEqual(c.position[2], 1);

            // modify face vertex
            a.position[0] = 1.1;
            a.position[1] = 0.1;
            a.position[2] = 0.2;
            Assert.AreEqual(mesh.get_vertex(1).position[0], 1.1);
            Assert.AreEqual(mesh.get_vertex(1).position[1], 0.1);
            Assert.AreEqual(mesh.get_vertex(1).position[2], 0.2);
        }

        [Test]
        public void TestGetEdge()
        {
            Edge e = mesh.get_edge(0);
            Assert.AreEqual(e.index, 0);
            Assert.AreEqual(e.vertices[0].index, 0);
            Assert.AreEqual(e.vertices[1].index, 1);
        }

        [Test]
        public void TestGetBoundary()
        {
            Assert.AreEqual(mesh.n_boundary_components(), 1);

            var b = mesh.get_boundary_component(0);
            Assert.AreEqual(b.n_vertices(), 3);
            Assert.AreEqual(b.n_edges(), 3);
            Assert.That(b.is_closed());

            // check edge 0
            Assert.That(b.get_vertex(0) == b.get_edge(0).get_vertex(0));
            Assert.That(b.get_vertex(1) == b.get_edge(0).get_vertex(1));

            // check edge 1
            Assert.That(b.get_vertex(1) == b.get_edge(1).get_vertex(0));
            Assert.That(b.get_vertex(2) == b.get_edge(1).get_vertex(1));

            // check edge 2
            Assert.That(b.get_vertex(2) == b.get_edge(2).get_vertex(0));
            Assert.That(b.get_vertex(0) == b.get_edge(2).get_vertex(1));
        }
    }
}