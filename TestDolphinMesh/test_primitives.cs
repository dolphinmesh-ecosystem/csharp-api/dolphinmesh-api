using NUnit.Framework;

using DolphinMesh;

namespace TestDolphinMesh
{
    public class TestPrimitives
    {

        [Test]
        public void TestView()
        {
            var x = new double[3];
            unsafe
            {
                fixed(double* p = &x[0])
                {
                    var a = new View<double>(p, 3);
                    var b = new View<double>(p, 3);

                    Assert.That(a == b);
                }
            }
        }


    }
}