using NUnit.Framework;

using DolphinMesh;

namespace TestDolphinMesh
{
    public class TestPolygonMesh
    {
        PolygonMesh mesh;

        [SetUp]
        public void Setup()
        {
            mesh = new PolygonMesh(6, 9, 1.2);

            mesh.add_vertex(0.0, 0.0, 0.0);  // 0
            mesh.add_vertex(1.0, 1.0, 0.0);  // 1
            mesh.add_vertex(2.0, 1.0, 0.0);  // 2
            mesh.add_vertex(3.0, 1.0, 0.0);  // 3
            mesh.add_vertex(4.0, 0.0, 0.0);  // 4
            mesh.add_vertex(3.0, -1.0, 0.0); // 5
            mesh.add_vertex(2.0, -1.0, 0.0); // 6
            mesh.add_vertex(1.0, -1.0, 0.0); // 7
            mesh.add_vertex(2.0, 0.0, 0.0);  // 8

            mesh.add_face(0, 7, 1);
            mesh.add_face(7, 8, 2, 1);
            mesh.add_face(7, 6, 8);
            mesh.add_face(6, 5, 2, 8);
            mesh.add_face(5, 3, 2);
            mesh.add_face(5, 4, 3);

            mesh.finalize();
        }


        [Test]
        public void TestCreation()
        {
            Assert.AreEqual(mesh.n_faces(), 6);
            Assert.AreEqual(mesh.n_vertices(), 9);
            Assert.AreEqual(mesh.n_edges(), 14);
        }

        [Test]
        public void TestTopology()
        {
            Assert.False(mesh.is_closed());
            Assert.AreEqual(mesh.n_genus(), 0);
            Assert.AreEqual(mesh.n_euler(), 1);
            Assert.AreEqual(mesh.n_boundary_components(), 1);
        }

        [Test]
        public void TestVertexGetter()
        {
            Vertex a = mesh.get_vertex(0);
            Assert.True(a.index == 0);
            Assert.AreEqual(a.position.x, 0);
            Assert.AreEqual(a.position.y, 0);
            Assert.AreEqual(a.position.z, 0);

            Vertex b = mesh.get_vertex(1);
            Assert.True(b.index == 1);
            Assert.AreEqual(b.position.x, 1);
            Assert.AreEqual(b.position.y, 1);
            Assert.AreEqual(b.position.z, 0);

            Vertex c = mesh.get_vertex(2);
            Assert.True(c.index == 2);
            Assert.AreEqual(c.position.x, 2);
            Assert.AreEqual(c.position.y, 1);
            Assert.AreEqual(c.position.z, 0);
        }

    }
}